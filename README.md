# Project Kava

Project Kava is an ongoing development effort to create a JavaME (J2ME) emulator for KaiOS mobile platform.
It emerged from the code base of the abandoned [JS2ME](https://github.com/szatkus/js2me) project for Firefox OS, also known as JayMe emulator, with Zazu engine left as the default and currently only viable option.

## Building and running

Current reference targets are:

- [KaiOS Simulator](https://developer.kaiostech.com/simulator) aka Kaiosrt;
- Nokia 8110 4G (developer mode) + WebIDE;
- Nokia 8110 4G (privileged mode) + [OmniSD](http://omnijb.831337.xyz).

To deploy Kava onto KaiOS Simulator or Nokia 8110 4G in the Developer Mode, no additional building is required.

For Kaiosrt development, just open the source tree directory in the simulator as a packaged app project.

For WebIDE based development on a real device, you'll need to use ADB and Firefox 52.9esr. Also, you'll need to run `adb forward tcp:6000 localfilesystem:/data/local/debugger-socket` command every time you connect the device using the `localhost:6000` remote runtime option, and probably might need to reboot your device with `adb reboot` every time after disconnection to be able to connect again.

To create an OmniSD package from current source tree, run the `pack-to-omni.sh` Bash script from the root project directory, and `kava-omni.zip` file should appear. Then transfer it into the '/apps' directory on your SD card, and OmniSD will see the package and you'll be able to install it.

After the app has started, press the middle joypad key or Send (Call) key to scan your storage area and pick a JAR file to run it.

## Configuration

### Screen modes

Despite Kava is set to support a single screen resolution (240x320) and runs in the full screen itself, it can run in three modes which you can switch between with "#" key **before** selecting a JAR file.
These screen modes are:

- `normal` - both titlebar and action button line are displayed;
- `half` - only action button line is displayed;
- `full` - just an active screen area is displayed (useful for games and apps with their own rendering engine, like Opera Mini).

The `half` mode was created because lots of J2ME applications don't use the title bar properly and even use it to store some internal variable data.

The default mode can also be persisted in the `manifest.webapp` by adding the `?screen=[mode]` query parameter to the `launch_path` like the following:
```
"launch_path": "/index.html?screen=half"
```
If the `screen` query parameter is omitted, default mode is `normal`.

### Predefined J2ME apps

You can also use Kava as a wrapper to port single JAR files to KaiOS, so that users could run them like native apps without having to pick anything. To do this, starting with a clean Kava source tree, you need to:

1. Copy a JAR file anywhere inside the source tree, e.g. into `jars/game.jar`;
2. Add a `src` query parameter with this **relative** JAR path you created to the `launch_path` in the `manifest.webapp` file like this: `"launch_path": "/index.html?screen=full&src=jars/game.jar"`;
3. Update the `name`, `description`, `origin`, `developer`, `icons` and `locales` fields accordingly in the `manifest.webapp` file.

## Current support status

This section is yet to be updated. For current feature support status, see the version history and roadmap sections.

## Roadmap

- 0.0.1: _(done)_ Initial functional port of JS2ME with most basic UI (viewport, text, keypad, 2D graphics, command buttons, command menus) adapted to KaiOS
- 0.0.5: Should be able run basic apps with more standard MicroEdition UI elements like text inputs or selectboxes
- 0.1.0: Command button bugs for some apps should be fixed
- 0.1.5: File I/O should work (as defined by MIDP-2.0)
- 0.2.0: Network I/O should work (as defined by MIDP-2.0)
- 0.3.0: JVM should run in the background (worker capability from JS2ME should be restored)
- 0.5.0: Complex apps like Opera Mini 4 should work as expected
- 0.6.0: Sound engine (MIDI support) should be implemented
- 1.0.0: All standard features defined by MIDP-2.0 specification must be supported

## Version history

- 0.0.1 (2018-12-31): Initial functional port that runs simplest 2D games like Asteroids.jar and g2048.jar, as well as some versions of Mobile Basic. Happy New Year!

## Credits

Original JS2ME/JayMe code written by: [Tomasz Szatkowski](http://www.szatkus.pl)

Initial KaiOS port done by: [Luxferre](http://831337.xyz)

Ongoing development done by: [Bananahackers group](https://sites.google.com/view/bananahackers/home)
